<?php
require '../../php/include.php';
require '../../php/Slim/Slim.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();

$app->post('/content/get', 'contentGet');
$app->post('/content/getAllPages', 'getAllPages');

/** Функционал заявок */
$app->post('/participants/getAll', 'participantsGetAll');
$app->post('/participants/getById', 'participantsGetById');
$app->post('/participants/getByUserId', 'participantsGetByUserId');
$app->post('/participants/getByStatus', 'participantsGetByStatus');
$app->post('/participants/getByType', 'participantsGetByType');
$app->post('/participants/insert', 'participantsInsert');
$app->post('/participants/updateStatus', 'participantsUpdateStatus');
$app->post('/participants/delete', 'participantsDelete');

/** Функционал обратной связи */
$app->post('/feedback/getAll', 'feedbackGetAll');
$app->post('/feedback/insert', 'feedbackInsert');

/** Функционал новостей */
$app->post('/news/getAll', 'newsGetAll');
$app->post('/news/getById', 'newsGetById');
$app->post('/news/insert', 'newsInsert');
$app->post('/news/update', 'newsUpdate');
$app->post('/news/delete', 'newsDelete');

/** Пользовательский функционал */
$app->post('/user/register', 'userRegister');

/** Работа с токенами */
$app->post('/token/auth', 'tokenAuth');
$app->post('/token/refresh', 'tokenRefresh');
$app->post('/token/check', 'tokenCheck');

$app->run();

function getData() {
    $request = \Slim\Slim::getInstance()->request();
    return json_decode($request->getBody());
}


function contentGet() {
    $data = getData();
    $page = $data->page;

    Content::getContent($page);
}

function getAllPages() {
    Content::getPagesList();
}


/**
 * Реализация функционала заявок
 */

/** Получение всех заявок */
function participantsGetAll() {
    Content::participantsGetAll();
}

/** Получение заявок по ID */
function participantsGetById() {
    $data = getData();
    $id = $data->id;

    Content::participantsGetById($id);
}

/** Получение заявок по ID пользователя */
function participantsGetByUserId() {
    $data = getData();
    $id = $data->userId;

    Content::participantsGetByUserId($id);
}

/** Получение заявок по статусу */
function participantsGetByStatus() {
    $data = getData();
    $status = $data->status;

    Content::participantsGetByStatus($status);
}

/** Получение заявок по типу */
function participantsGetByType() {
    $data = getData();
    $type = $data->type;

    Content::participantsGetByType($type);
}

/** Добавление заявки */
function participantsInsert() {
    $data = getData();

    $participant = new Participant(
        $data->fullCompanyName,
        $data->website,
        $data->inn,
        $data->kpp,
        $data->bik,
        $data->pc,
        $data->ks,
        $data->bank,
        $data->companyLegalAddress,
        $data->companyMailAddress,
        $data->companyPhone,
        $data->companyFax,
        $data->companyEmail,
        $data->guideFio,
        $data->responsibleFio,
        $data->responsibleEmail,
        $data->responsiblePosition,
        $data->responsiblePhone,
        $data->typeOfProduct,
        $data->numOfProduct,
        $data->width,
        $data->length,
        $data->electricityConsumed,
        $data->wishes,
        $data->type,
        $data->userId
    );

    Content::participantsInsert($participant);
}

/** Обновление статуса заявки */
function participantsUpdateStatus() {
    $data = getData();

    $id = $data->id;
    $status = $data->status;

    Content::participantsUpdateStatus($id, $status);
}

/** Удаление заявки по ID */
function participantsDelete() {
    $data = getData();

    $id = $data->id;

    Content::participantsDelete($id);
}


/**
 * Реализация функционала новостей
 */

function feedbackGetAll() {
    Content::feedbackGetAll();
}

function feedbackInsert() {
    $data = getData();

    $date = date("Y-m-d");
    $feedback = new Feedback(
        $data->name,
        $data->email,
        $data->message,
        $date
    );

    Content::feedbackInsert($feedback);
}


/**
 * Реализация функционала новостей
 */

/** Получение всех новостей */
function newsGetAll() {
    Content::newsGetAll();
}

/** Получение новости по ID */
function newsGetById() {
    $data = getData();

    $id = $data->id;

    Content::newsGetById($id);
}

/** Добавление новости */
function newsInsert() {
    $data = getData();

    $date = date("Y-m-d");

    $post = new Post(
        $data->title,
        $data->description,
        $data->text,
        $date,
        $data->author
    );

    Content::newsInsert($post);
}

/** Обновление новости */
function newsUpdate() {
    $data = getData();

    $post = new Post(
        $data->title,
        $data->description,
        $data->text,
        $data->date,
        $data->author
    );
    $post->setId($data->id);

    Content::newsUpdate($post);
}

/** Удаление новости по ID */
function newsDelete() {
    $data = getData();

    $id = $data->id;

    Content::newsDelete($id);
}


/**
 * Реализация пользовательского функционала
 */

/** Регистрация пользователя */
function userRegister() {
    $data = getData();

    $email = $data->email;
    $password = $data->password;
    $passwordTwice = $data->check;
    $name = $data->name;
    $surname = $data->surname;

    if ($password === $passwordTwice) {
        Auth::register($email, $password, $name, $surname);
    }
    else {
        echo json_encode(['error' => 'Passwords do not match!']);
    }
}


/**
 * Реализация токенов
 */

/** Авторизация пользователя и получение токенов доступа и сброса */
function tokenAuth() {
    $data = getData();

    $email = $data->email;
    $password = $data->password;

    Access::getAccess($email, $password);
}

/** Обновление токенов с помощью токена сброса, получение новых токенов доступа и сброса */
function tokenRefresh() {
    $data = getData();

    $refresh_token = $data->refresh_token;

    Access::getRefresh($refresh_token);
}

/** Проверка токенов на актуальность */
function tokenCheck() {
    $data = getData();

    $access_token = $data->access_tokem;
    $refresh_token = $data->refresh_token;

    Access::checkTokens($access_token, $refresh_token);
}