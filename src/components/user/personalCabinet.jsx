import React, { Component } from 'react';
import { MDBBtn, MDBIcon, MDBInput } from 'mdbreact'
import { postOrder } from '../../services/postData';

export default class PersonalCabinet extends Component {
    constructor(props){
        super(props)

        this.state = {
            userId: this.props.user.uid,
            type: "simple"
        }

        this.onSubmit = this.onSubmit.bind(this);
        this.changeData = this.changeData.bind(this);
    }

    changeData(e){
        this.setState({[e.target.name]: e.target.value});
    }

    onSubmit(e){
        e.preventDefault();
        console.log(this.state)
        postOrder(this.state).then((res) => {
            console.log(res)
        })
        alert("Ваша заявка отправлена!")
    }

    render() {
        return (
            <section className="py-5">
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-md-10 offset-md-1">
                            <h4 className="section__heading">Отправленные заявки</h4>
                            <div className="table-responsive">
                                <table className="table">
                                    <tr>
                                        <td>Номер заявки</td>
                                        <td>Статус</td>
                                        <td>Скачать</td>
                                        <td>Время отправления</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 col-md-10 offset-md-1">
                            <h4 className="section__heading">Новая заявка</h4>
                            <form onSubmit={this.onSubmit}>
                                <div className="table-responsive">
                                    <table className="table">
                                        <tr>
                                            <td colSpan="2">
                                                <MDBInput
                                                    label="Полное наименование компании"
                                                    name="fullCompanyName"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                            <td>
                                                <MDBInput
                                                    label="Сайт"
                                                    name="website"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <MDBInput
                                                    label="ИНН"
                                                    name="inn"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                            <td>
                                                <MDBInput
                                                    label="КПП"
                                                    name="kpp"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                            <td>
                                                <MDBInput
                                                    label="БИК"
                                                    name="bik"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <MDBInput
                                                    label="Р.С"
                                                    name="pc"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                            <td>
                                                <MDBInput
                                                    label="К.С"
                                                    name="ks"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                            <td>
                                                <MDBInput
                                                    label="Наименование банка"
                                                    name="bank"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colSpan="3">
                                                <MDBInput
                                                    label="Юридический адрес"
                                                    name="companyLegalAddress"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colSpan="3">
                                                <MDBInput
                                                    label="Почтовый/Фактический адреc"
                                                    name="companyMailAddress"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <MDBInput
                                                    label="Тел."
                                                    name="companyPhone"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                            <td>
                                                <MDBInput
                                                    label="Факс"
                                                    name="companyFax"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                            <td>
                                                <MDBInput
                                                    label="E-mail"
                                                    name="companyEmail"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colSpan="3">
                                                <MDBInput
                                                    label="Ф.И.О. руководителя"
                                                    name="guideFio"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colSpan="2">
                                                <MDBInput
                                                    label="Ответственное лицо за участие в фестивале Ф.И.О"
                                                    name="responsibleFio"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                            <td colSpan="1">
                                                <MDBInput
                                                    label="E-mail"
                                                    name="responsibleEmail"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colSpan="2">
                                                <MDBInput
                                                    label="Должность"
                                                    name="responsiblePosition"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                            <td colSpan="1">
                                                <MDBInput
                                                    label="Телефон"
                                                    name="responsiblePhone"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                <MDBInput
                                                    label="Вид продукции"
                                                    name="typeOfProduct"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                            <td>
                                                <MDBInput
                                                    label="Кол-во торговых точек"
                                                    name="numOfProduct"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                            <td>
                                                <MDBInput
                                                    label="Ширина, м"
                                                    name="width"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                            <td>
                                                <MDBInput
                                                    label="Длина, м"
                                                    name="length"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colSpan="4">
                                                <MDBInput
                                                    label="Кол-во потребляемой электроэнергии (кВт/час)"
                                                    name="electricityConsumed"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colSpan="4">
                                                <MDBInput
                                                    label="Примечание и особые пожелания"
                                                    name="wishes"
                                                    group
                                                    type="text"
                                                    validate
                                                    error="wrong"
                                                    success="right"
                                                    onChange={this.changeData}
                                                />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <MDBBtn color="danger" outline type="submit">
                                    Отправить
                                    <MDBIcon far icon="paper-plane" className="ml-2" />
                                </MDBBtn>
                            </form>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 col-md-6 offset-md-3">

                        </div>
                    </div>
                </div>
            </section>
        );
    }
}