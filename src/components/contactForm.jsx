import React, { Component } from 'react';
import { MDBBtn, MDBIcon, MDBInput } from 'mdbreact';

export default class ContactForm extends Component {
    render() {
        return (
            <form>
                <MDBInput
                    label="Ваше имя"
                    icon="user"
                    group
                    type="text"
                    validate
                    error="wrong"
                    success="right"
                />
                <MDBInput
                    label="Ваш email"
                    icon="envelope"
                    group
                    type="email"
                    validate
                    error="wrong"
                    success="right"
                />
                <MDBInput
                    type="textarea"
                    rows="2"
                    label="Ваше сообщение"
                    icon="pencil-alt"
                />
                <div className="text-right mt-4">
                    <MDBBtn color="danger" outline type="submit">
                        Отправить
                        <MDBIcon far icon="paper-plane" className="ml-2" />
                    </MDBBtn>
                </div>
            </form>
        )
    }
}