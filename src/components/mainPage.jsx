import React, { Component } from 'react';

import PhotoSlider from './slider';
import SponsorsSlider from './sponsorsSlider';
import Map from './map';
import ContactForm from './contactForm';

import { MDBBtn } from "mdbreact";


class Content extends Component {
    constructor(props) {
        super(props)
        this.state = {
            mainPage: this.props.content,
            isLoaded: true
        }
    }

    componentDidMount() {
        this.setState({ "isLoaded": true })
    }

    render() {
        if (this.state.isLoaded) {
            return (
                <>
                    <section className="page__section first__section py-5">
                        <div className="container">
                            <div className="col-12 d-flex flex-column align-items-start align-items-md-center">
                                <h1 className="font-weight-bold word-wrap first__section__heading" color="danger">{this.state.mainPage[0].content}</h1>
                                <h3 className="first__section__subheading">{this.state.mainPage[1].content}</h3>
                                <p className="first__section__subheading">{this.state.mainPage[2].content}</p>
                                <MDBBtn className="first__section__button" outline color="danger">{this.state.mainPage[3].content}</MDBBtn>
                            </div>
                        </div>
                    </section>
                    <section className="d-flex flex-column pt-5 pb-4">
                        <div className="d-flex flex-column flex-sm-row">
                            <div className="col-12 col-sm-4 text-center mb-3 mb-md-0 statistic">{this.state.mainPage[4].content}</div>
                            <div className="col-12 col-sm-4 text-center mb-3 mb-md-0 statistic">{this.state.mainPage[5].content}</div>
                            <div className="col-12 col-sm-4 text-center mb-3 mb-md-0 statistic">{this.state.mainPage[6].content}</div>
                        </div>
                        <div className="container mt-4">
                            <div className="buttos d-flex flex-md-row flex-wrap justify-content-center col-12">
                                <div className="col-12 col-md-6">
                                    <MDBBtn className="statistic__button w-100 w-md-auto mx-0" color="danger" size="md">Презентация</MDBBtn>
                                </div>
                                <div className="col-12 col-md-6">
                                    <MDBBtn className="statistic__button w-100 w-md-auto mx-0" color="danger" size="md">Заявка на участие</MDBBtn>
                                </div>
                                <div className="col-12">
                                    <MDBBtn className="statistic__button w-100 w-md-auto mx-0" color="danger" size="md">Заявка на участие в программе</MDBBtn>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="py-4">
                        <div className="container">
                            <div className="col-12 col-md-10 offset-md-1 mb-4 colored py-4">
                                <p>
                                    Гастрономический фестиваль «ГРЕНАДИН» состоится 17-18 августа 2019 г.
                                    Место проведения: г. Москва, ПКиО «Сокольники».
                            </p>
                                <p>
                                    Это праздник вкуснейшей еды, десертов и напитков, познавательных
                                    мастер-классов, конкурсов и развлечений.
                            </p>
                                <p>
                                    Фестиваль откроется костюмированным карнавальным шествием
                                    участников и посетителей мероприятия. На Фонтанной площади
                                    и прогулочных аллеях парка разместятся точки питания различных
                                    производителей и продавцов гастрономической продукции.
                                    Концертно-развлекательная программа на главной сцене
                                    Гастрономического фестиваля "ГРЕНАДИН" создаст особую атмосферу праздника.
                            </p>
                            </div>
                            <h4 className="text-center">ВХОД СВОБОДНЫЙ!</h4>
                        </div>
                    </section>
                    <section className="py-3">
                        <div className="container">
                            <div className="col-12 col-md-10 offset-md-1">
                                <h4 className="section__heading">Ассортимент</h4>
                                <ul>
                                    <li>мясная, молочная, рыбная, кондитерская, хлебо-булочная, диетическая продукция;</li>
                                    <li>детское питание;</li>
                                    <li>эко продукты(мед, орехи, злаки);</li>
                                    <li>ресторанная, уличная, вегетарианская, национальная кухня;</li>
                                    <li>снеки, сладости, мороженое;</li>
                                    <li>холодные и горячие напитки;</li>
                                    <li>спортивное питание;</li>
                                    <li>гастрономические растения (пряные, цветочные, ягодные, плодовые);</li>
                                    <li>приправы и специи;</li>
                                    <li>кухонные принадлежности и бытовые приборы.</li>
                                </ul>
                            </div>
                        </div>
                    </section>
                    <section className="py-3">
                        <div className="container">
                            <div className="col-12 col-md-10 offset-md-1">
                                <h4 className="section__heading">Посетители</h4>
                                <p>
                                    Фестиваль заинтересует широкую аудиторию москвичей и гостей столицы любого возраста
                                    и вкусовых предпочтений.
                            </p>
                                <p>
                                    Посетители парка станут участниками замечательного праздника еды и развлечений.
                            </p>
                                <p>
                                    Ведущие кулинары и бармены поделятся секретами вкуснейших рецептов  и способов
                                    подачи блюд и напитков.
                            </p>
                            </div>
                        </div>
                    </section>
                    <PhotoSlider />
                    <section className="py-4">
                        <div className="container">
                            <div className="col-12 col-md-10 offset-md-1">
                                <h4 className="section__heading">Участники</h4>
                                <SponsorsSlider sponsors={[1, 2, 3, 4]} />
                            </div>
                        </div>
                    </section>
                    <section className="py-4">
                        <div className="container d-flex flex-column flex-md-row">
                            <div className="col-12 col-md-5 offset-md-1 mb-4 mb-md-0">
                                <h4 className="section__heading">Контакты</h4>
                                <Map />
                            </div>
                            <div className="col-12 col-md-5 offset-md-1">
                                <h4 className="section__heading">Задать вопрос</h4>
                                <ContactForm />
                            </div>
                        </div>
                    </section>
                </>
            );
        } else {
            return (
                <div>preloader</div>
            )
        }
    }
}

export default Content