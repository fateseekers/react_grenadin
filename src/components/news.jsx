import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { MDBIcon } from "mdbreact";
import img from '../media/img/1.jpg'

export default class News extends Component {
    render() {
        return (
            <section className="py-4">
                <div className="container">
                    <div className="col-12 col-md-10 offset-md-1">
                        <h4 className="section__heading">Новости</h4>
                        <div className="news__card d-flex flex-column flex-md-row">
                            <div className="col-12 col-md-5">
                                <img className="news__img w-100 my-3" src={img} alt="" />
                            </div>
                            <div className="col-12 col-md-8">
                                <div className="news__short_info d-flex flex-column">
                                    <h5 className="news__heading">Невероятно вкусные бургеры от шефа</h5>
                                    <p className="news__text">Вот такой вот мужчина держит в руках булочку и показывает два пальца, таким образом люди обычно здороваются. Вот такой вот мужчина держит в руках булочку и показывает два пальца, таким образом люди обычно здороваются...</p>
                                    <Link className="news__link d-flex align-items-center" to="/current_news/1">Подробнее <MDBIcon className="ml-2" icon="angle-double-right" /></Link>
                                </div>
                            </div>
                        </div>
                        <div className="news__card d-flex flex-column flex-md-row">
                            <div className="col-12 col-md-5">
                                <img className="news__img w-100 my-3" src={img} alt="" />
                            </div>
                            <div className="col-12 col-md-8">
                                <div className="news__short_info d-flex flex-column">
                                    <h5 className="news__heading">Невероятно вкусные бургеры от шефа</h5>
                                    <p className="news__text">Вот такой вот мужчина держит в руках булочку и показывает два пальца, таким образом люди обычно здороваются. Вот такой вот мужчина держит в руках булочку и показывает два пальца, таким образом люди обычно здороваются...</p>
                                    <Link className="news__link d-flex align-items-center" to="/current_news/1">Подробнее <MDBIcon className="ml-2" icon="angle-double-right" /></Link>
                                </div>
                            </div>
                        </div>
                        <div className="news__card d-flex flex-column flex-md-row">
                            <div className="col-12 col-md-5">
                                <img className="news__img w-100 my-3" src={img} alt="" />
                            </div>
                            <div className="col-12 col-md-8">
                                <div className="news__short_info d-flex flex-column">
                                    <h5 className="news__heading">Невероятно вкусные бургеры от шефа</h5>
                                    <p className="news__text">Вот такой вот мужчина держит в руках булочку и показывает два пальца, таким образом люди обычно здороваются. Вот такой вот мужчина держит в руках булочку и показывает два пальца, таким образом люди обычно здороваются...</p>
                                    <Link className="news__link d-flex align-items-center" to="/current_news/1">Подробнее <MDBIcon className="ml-2" icon="angle-double-right" /></Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}