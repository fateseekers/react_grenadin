import React, { Component } from 'react';
import Slider from "react-slick";
import "../css/slick-theme.css";
import "../css/slick.css";

import img from '../media/img/1.jpg'
export default class Sponsors_Slider extends Component {
    constructor(props){
        super(props);
        this.state = {
            sponsors: this.props.sponsors   
        }
        // console.log(this.state)
    }


    render() {
        const sponsors__slider__settings = {
            className: "sponsors__slider variable-width",
            arrows: false,
            dots: false,
            infinite: true,
            centerMode: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: true,
            autoplay: true,
            speed: 2000,
            autoplaySpeed: 4000
        };
        return (
            <>
                <Slider key="sponsor" {...sponsors__slider__settings}>
                    <div className="d-flex">
                        {/* <img src={img}  alt="" />
                        <img src={img}  alt="" />
                        <img src={img}  alt="" />
                        <img src={img}  alt="" />
                        <img src={img}  alt="" /> */}
                    </div>
                </Slider>
            </>
        )
    }
}