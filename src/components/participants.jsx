import React, { Component } from 'react';
import { MDBBtn } from 'mdbreact';

export default class Participants extends Component {
    constructor(props) {
        super(props);

        this.toggleTabs = this.toggleTabs.bind(this);
    }

    toggleTabs(e) {
        let tabs_content = document.getElementsByClassName('tabs__info');
        let index = Array.prototype.indexOf.call(e.target.parentElement.children, e.target);
        for (let i = 0; i < tabs_content.length; i++) {
            tabs_content[i].classList.add('d-none');
        }
        tabs_content[index].classList.remove('d-none')
    }

    render() {
        return (
            <section className="py-4">
                <div className="container">
                    <div className="col-12 col-md-10 offset-md-1 d-flex flex-column">
                        <div className="tabs d-flex flex-column flex-md-row justify-content-around mb-4">
                            <MDBBtn className="tabs__btn colored" color="" aria-label="tabs_1" onClick={this.toggleTabs}>Участникам</MDBBtn>
                            <MDBBtn className="tabs__btn colored" color="" aria-label="tabs_2" onClick={this.toggleTabs}>Схема площадки</MDBBtn>
                            <MDBBtn className="tabs__btn colored" color="" aria-label="tabs_3" onClick={this.toggleTabs}>Корнер</MDBBtn>
                        </div>
                        <div className="tabs__content mx-2">
                            <div id="tabs_1" className="tabs__info">
                                <h4 className="section__heading">Участникам</h4>
                                <p>Производители и продавцы:</p>
                                <ul>
                                    <li>мясной, молочной, рыбной, кондитерской, хлебо-булочной, диетической продукции;</li>
                                    <li>детского питания;</li>
                                    <li>эко продуктов (мед, орехи, злаки);</li>
                                    <li>ресторанной, уличной, вегетарианской, национальной кухни;</li>
                                    <li>снеков;</li>
                                    <li>сладостей;</li>
                                    <li>мороженого;</li>
                                    <li>холодных и горячих напитков;</li>
                                    <li>спортивного питания;</li>
                                    <li>гастрономических растений (пряных, цветочных, ягодных, плодовых);</li>
                                    <li>приправ и специй;</li>
                                    <li>кухонных принадлежностей и бытовых приборов.</li>
                                </ul>
                                <p>Организаторы игровых зон, мастер-классов для детей и взрослых.</p>
                            </div>
                            <div id="tabs_2" className="tabs__info d-none">
                                <h4 className="section__heading">Схема площадки</h4>
                            </div>
                            <div id="tabs_3" className="tabs__info d-none">
                                <h4 className="section__heading">Корнер</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}