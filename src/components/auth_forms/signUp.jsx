import React, { Component } from 'react';
import { MDBBtn, MDBInput } from 'mdbreact';
import { Link } from 'react-router-dom';
import { userRegister } from '../../services/postData';

export default class SignUp extends Component {
    constructor(props){
        super(props)

        this.state = {
            name: "",
            surname: "",
            email: "",
            password: "",
            check: ""
        }

        this.onSubmit = this.onSubmit.bind(this);
        this.changeData = this.changeData.bind(this);
    }

    changeData(e){
        this.setState({[e.target.name]: e.target.value});
    }

    onSubmit(e){
        e.preventDefault();
        userRegister(this.state).then((res) => {
            console.log(res)
        })
        alert("Вы зарегистрированы!")
    }

    render() {
        return (
            <section className="py-5">
                <div className="container">
                    <div className="col-12 col-md-6 offset-md-3">
                        <form onSubmit={this.onSubmit}>
                            <p className="section__heading h5 text-center mb-4">Регистрация</p>
                            <div className="grey-text">
                                <MDBInput
                                    label="Ваше имя"
                                    icon="user"
                                    group
                                    name="name"
                                    type="text"
                                    validate
                                    error="wrong"
                                    success="right"
                                    onChange={this.changeData}
                                />
                                <MDBInput
                                    label="Ваша фамилия"
                                    icon="address-book"
                                    group
                                    name="surname"
                                    type="text"
                                    validate
                                    error="wrong"
                                    success="right"
                                    onChange={this.changeData}
                                />
                                <MDBInput
                                    label="Ваш e-mail"
                                    icon="envelope"
                                    group
                                    name="email"
                                    type="email"
                                    validate
                                    error="wrong"
                                    success="right"
                                    onChange={this.changeData}
                                />
                                <MDBInput
                                    label="Ваш пароль"
                                    icon="lock"
                                    group
                                    name="password"
                                    type="password"
                                    validate
                                    onChange={this.changeData}
                                />
                                <MDBInput
                                    label="Подтвердите пароль"
                                    icon="exclamation-triangle"
                                    group
                                    name="check"
                                    type="password"
                                    validate
                                    onChange={this.changeData}
                                />
                            </div>
                            <div className="text-center d-flex flex-column justify-content-center">
                                <MDBBtn type="submit" className="colored px-1" color="">Зарегистрироваться</MDBBtn>
                                <Link to="/authorisation/signin">Уже зарегистрированы?</Link>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        )
    }
}