import React, { Component } from 'react';
import { MDBBtn, MDBInput } from 'mdbreact';
import { Link } from 'react-router-dom';
import { userLogin } from '../../services/postData';
import {connect} from 'react-redux';
import updateToken from '../../services/actions';

class SignIn extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: "",
            password: ""
        }

        this.onSubmit = this.onSubmit.bind(this);
        this.changeData = this.changeData.bind(this);
    }

    changeData(e){
        this.setState({[e.target.name]: e.target.value});
    }


    onSubmit(e) {
        e.preventDefault();
        console.log(this.state)
        userLogin(this.state).then((res) => {
            console.log(res)
            if (res.errno == 0) {
                this.props.toRedux({ "access": res.access, "refresh": res.refresh })
                this.props.history.push('/');
            }
        })
        
    }

    render() {
        return (
            <section className="py-5">
                <div className="container">
                    <div className="col-12 col-md-6 offset-md-3">
                        <form onSubmit={this.onSubmit}>
                            <p className="section__heading h5 text-center mb-4">Вход</p>
                            <div className="grey-text">
                                <MDBInput
                                    label="Ваш e-mail"
                                    icon="envelope"
                                    name="email"
                                    group
                                    type="email"
                                    validate
                                    error="wrong"
                                    success="right"
                                    onChange={this.changeData}
                                />
                                <MDBInput
                                    label="Ваш пароль"
                                    icon="lock"
                                    name="password"
                                    group
                                    type="password"
                                    validate
                                    onChange={this.changeData}
                                />
                            </div>
                            <div className="text-center d-flex flex-column justify-content-center">
                                <MDBBtn type="submit" className="colored" color="">Войти</MDBBtn>
                                <Link to="/authorisation/signup">Зарегистрироваться</Link>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        )
    }
}

export default connect(
    state =>({token: state.token}),
    dispatch => ({toRedux: (data)=> {dispatch(updateToken(data))}}))(SignIn)