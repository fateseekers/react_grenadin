import React, { Component } from 'react';
import { getParticipants } from '../services/postData';

export default class AdminPanel extends Component {
    constructor(props) {
        super(props)

        this.state = {}
    }

    async componentDidMount() {
        this.setState({ 'participants': await getParticipants() })
    }

    render() {
        if (this.state.participants) {
            return (
                <section className="py-5">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-md-10 offset-md-1">
                                <h4 className="section__heading">Отправленные заявки</h4>
                                <div className="table-responsive">
                                    <table className="table">
                                        <tr>
                                            <td>Номер заявки</td>
                                            <td>Статус</td>
                                            <td>Скачать</td>
                                            <td>Время отправления</td>
                                        </tr>
                                        {console.log(this.state.participants)}
                                        {this.state.participants.map((order) => {
                                            return (
                                                <tr>
                                                    <td>{order.id}</td>
                                                    <td>{order.status}</td>
                                                    <td>Скачать</td>
                                                    <td>{order.updated_at}</td>
                                                </tr>
                                            )
                                        })}
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            )
        } else {
            return (
                <div>loading orders</div>
            )
        }
    }
}