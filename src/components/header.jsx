import React, { Component } from 'react';
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler, MDBCollapse, MDBDropdown, MDBDropdownToggle, MDBDropdownMenu, MDBDropdownItem, MDBIcon } from "mdbreact";
export default class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      token: this.props.user,
      isOpen: false
    };
  }

  toggleCollapse = () => {
    this.setState({ isOpen: !this.state.isOpen });
  }

  componentDidMount() {
    console.log(this.state.token)
  }

  render() {
    return (
      <MDBNavbar color="danger-color-dark" dark expand="md">
        <MDBNavbarBrand>
          <strong className="white-text">Navbar</strong>
        </MDBNavbarBrand>
        <MDBNavbarToggler onClick={this.toggleCollapse} />
        <MDBCollapse id="navbarCollapse3" isOpen={this.state.isOpen} navbar>
          <MDBNavbarNav right>
            <MDBNavItem>
              <MDBNavLink to="/">Главная</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink to="/participants">Участникам</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink to="/sponsors">Спонсорам</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink to="/news">Новости</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink to="/contacts">Контакты</MDBNavLink>
            </MDBNavItem>
          </MDBNavbarNav>
          <MDBNavbarNav right>
            <MDBNavItem className="d-flex">
              <MDBNavLink className="waves-effect waves-light" to="https://vk.com/feed">
                <MDBIcon fab icon="vk" />
              </MDBNavLink>
              <MDBNavLink className="waves-effect waves-light" to="#!">
                <MDBIcon fab icon="instagram" />
              </MDBNavLink>
            </MDBNavItem>
            {!this.state.token.role ?
              <MDBNavItem>
                <MDBNavLink to="/authorisation/signin">Вход</MDBNavLink>
              </MDBNavItem>
              : null}
            {this.state.token.role ?
              <MDBNavItem className="">
                <MDBDropdown dropright drop-md-left>
                  <MDBDropdownToggle nav caret>
                    <MDBIcon icon="user" />
                  </MDBDropdownToggle>
                  <MDBDropdownMenu className="dropdown-default">
                    {this.state.token.role === "user"
                      ? <MDBDropdownItem>
                        <MDBNavLink to="/cabinet">Личный кабинет</MDBNavLink>
                      </MDBDropdownItem>
                      : <MDBDropdownItem>
                        <MDBNavLink to="/panel" className="color-dark">Управление</MDBNavLink>
                      </MDBDropdownItem>}
                    <MDBDropdownItem>
                      <MDBNavLink to="#!" className="color-dark">Выход</MDBNavLink>
                    </MDBDropdownItem>
                  </MDBDropdownMenu>
                </MDBDropdown>
              </MDBNavItem>
              : null}
          </MDBNavbarNav>
        </MDBCollapse>
      </MDBNavbar>
    )
  }
}