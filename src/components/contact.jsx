import React, { Component } from 'react';
import Map from './map';
import ContactForm from './contactForm';

export default class Contact extends Component {
    render() {
        return (
            <section className="py-4">
                <div className="container">
                    <div className="col-12 col-md-10 offset-md-1 d-flex flex-column">
                        <div className="d-flex flex-column flex-md-row mb-4">
                            <div className="col-12 col-md-6">
                                <h4 className="section__heading">Контакты</h4>
                                <Map />
                            </div>
                            <div className="col-12 col-md-6">
                                <h4 className="section__heading">Связаться с нами</h4>
                                <ContactForm />
                            </div>
                        </div>
                        <div className="col-12">
                            <h4 className="section__heading">Информация о нас</h4>
                            <p>Организатор: ООО "АСПЕКТ"</p>
                            <p>+ 7 977 895-60-77</p>
                            <p>+ 7 985 415-59-75</p>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}