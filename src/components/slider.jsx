
import Slider from "react-slick";
import "../css/slick-theme.css";
import "../css/slick.css";
import React, { Component } from "react";
import first from '../media/img/1.jpg';


export default class Photo_Slider extends Component {
    render() {
        const photo__slider__settings = {
            className: "photo__slider variable-width",
            arrows: false,
            dots: false,
            infinite: true,
            centerMode: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: true,
            autoplay: true,
            speed: 2000,
            autoplaySpeed: 4000
        };
        return (
            <section>
                <Slider key="photo" {...photo__slider__settings}>
                    <div>
                        <img src={first} alt="" />
                    </div>
                    <div>
                        <img src={first} alt="" />
                    </div>
                    <div>
                        <img src={first} alt="" />
                    </div>
                </Slider>
            </section>
        )
    }
}