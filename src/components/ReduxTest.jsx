import React, {Component} from 'react';
import {connect} from 'react-redux'
import updateToken from "../services/actions";
class ReduxTest extends Component{
    onClickClack = (data) => {
        this.props.toRedux(data);
    };
    render() {
        return(
            <div>
                <div>{this.props.accessToken.updated_token}</div>
                <button onClick={()=>this.onClickClack("работает")}>Do something</button>
            </div>
        )
    }
}
export default connect(state =>({accessToken: state.accessToken}),
    dispatch => ({toRedux: (data)=> {dispatch(updateToken(data))}}))(ReduxTest)
