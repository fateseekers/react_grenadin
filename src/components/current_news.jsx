import React, { Component } from 'react';

export default class CurrentNews extends Component{
    constructor(props){
        super(props);

        this.state = {
            currentId: this.props.match.params.id
        }
    }
    render(){
        return(
            <div>Текущая новость {this.state.currentId}</div>
        )
    }
} 