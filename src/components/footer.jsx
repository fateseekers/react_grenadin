import React, { Component } from 'react';

export default class Footer extends Component {
    render() {
        return (
            <>
            <footer className="footer colored pb-3">
                <div className="container">
                    <div className="col-12 col-md-10 offset-md-1 d-flex flex-column flex-md-row">
                        <div className="col-12 col-md-6 py-3 d-flex flex-column">
                            <p>Организатор: ООО "АСПЕКТ"</p>
                            <p>тел.: <a className="text-white" href="tel:+79778956077">+7 977 895-60-77</a>;<br/><a className="text-white" href="tel:+79854155975">+7 985 415-59-75</a></p>
                            <p>e-mail: <a className="text-white" href="mailto:office@aspect.msk.ru">office@aspect.msk.ru</a></p>
                        </div>
                        <div className="col-12 col-md-6 py-3 d-flex flex-column">
                            2
                        </div>
                    </div>
                </div>
            </footer>
            
            </>
        )
    }
}