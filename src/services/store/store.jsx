import { createStore } from 'redux';
import mainReducer from '../reducers/index';

export const defaultStore = {
    token: {
        accessToken: localStorage.getItem("accessToken"),
        refreshToken: localStorage.getItem("refreshToken")
    }
};

export default createStore( mainReducer, defaultStore, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())