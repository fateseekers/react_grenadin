const url = "http://react-grenadin";

export function postOrder(userData) {
    return new Promise((resolve, reject) => {
        fetch(url + "/slim/participants/insert", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userData)
        })
        .then((res) => res.json())
        .then((res) => {
            resolve(res);
        })
        .catch((error) => {
            reject(error);
        });
    });
}

export function getContent() {
    return new Promise((resolve, reject) => {
        fetch(url + "/slim/content/getAllPages", {
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Accept": "application/json",
                "Content-Type": "application/json"
            }
        })
        .then((res) => res.json())
        .then((res) => {
            resolve(res);
        })
        .catch((error) => {
            reject(error);
        });
    });
}

export function getParticipants() {
    return new Promise((resolve, reject) => {
        fetch(url + "/slim/participants/getAll", {
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Accept": "application/json",
                "Content-Type": "application/json"
            }
        })
        .then((res) => res.json())
        .then((res) => {
            resolve(res);
        })
        .catch((error) => {
            reject(error);
        });
    });
}


export function userRegister(userData) {
    return new Promise((resolve, reject) => {
        fetch(url + "/slim/user/register", {
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify(userData)
        })
        .then((res) => res.json())
        .then((res) => {
            resolve(res);
        })
        .catch((error) => {
            reject(error);
        });
    });
}

export function userLogin(userData) {
    return new Promise((resolve, reject) => {
        fetch(url + "/slim/token/auth", {
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify(userData)
        })
        .then((res) => res.json())
        .then((res) => {
            resolve(res);
        })
        .catch((error) => {
            reject(error);
        });
    });
}