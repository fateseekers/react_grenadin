function token(state = {}, action){
    switch(action.type){
        case "GET_TOKEN":
            console.log("GET_TOKEN");
            return state.merge({"get_token": true});
        case "UPDATE_TOKEN":
            console.log("UPDATE_TOKEN + " + action.payload);
            localStorage.setItem("accessToken", action.payload.access);
            localStorage.setItem("refreshToken", action.payload.refresh);
            return {"accessToken": action.payload.access, "refreshToken": action.payload.refresh};
        default:
            return state;
    }
}

export default token;