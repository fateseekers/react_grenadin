import React, { Component } from 'react';
import { Switch, Route } from 'react-router';
import { BrowserRouter, Redirect } from 'react-router-dom';

import './App.css';
import Header from './components/header'


import MainPage from './components/mainPage';
import Footer from './components/footer';
import News from './components/news';

import { getContent } from './services/postData';
import Participants from './components/participants';
import Sponsors from './components/sponsors';
import Contact from './components/contact';
import CurrentNews from './components/current_news';
import SignIn from './components/auth_forms/signIn';
import SignUp from './components/auth_forms/signUp';


import { connect } from 'react-redux'
import updateToken from './services/actions/';
import base64 from 'react-native-base64';
import PersonalCabinet from './components/user/personalCabinet';
import Error404 from './errors/404';
import AdminPanel from './components/adminPanel';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      isLoaded: false,
      contains: [],
      mainPage: []
    }
  }

  async componentDidMount() {
    this.setState({ 'contains': await getContent() });

    this.state.contains.map((item) => {
      switch (item.page) {
        case "/":
          let arr = this.state.mainPage;
          arr.push(item)
          this.setState({ mainPage: arr });
          break;
        default:
          break;
      }
    })
    if (localStorage.getItem("accessToken") !== (undefined || "" || null)) {
      console.log(localStorage.getItem("accessToken"))
      let token = localStorage.getItem("accessToken");
      let arr = token.split('.');
      this.setState({ "token": JSON.parse(base64.decode(arr[1])) });
      console.log(this.state.token)
    }
    this.setState({ "isLoaded": true })
  }



  render() {
    if (this.state.isLoaded) {
      return (
        <BrowserRouter>
          <>
            <Header user={this.state.token}/>
            <Switch>
              <Route exact path="/" render={() => <MainPage user={this.state.token} content={this.state.contains} />} />
              <Route path="/news" component={News} />
              <Route path="/current_news/:id" component={CurrentNews} />
              <Route path="/participants" component={Participants} />
              <Route path="/sponsors" component={Sponsors} />
              <Route path="/contacts" component={Contact} />
              <Route path="/authorisation/signin" component={SignIn} />
              <Route path="/authorisation/signup" component={SignUp} />
              <Route
                path="/cabinet"
                render={(props) => this.state.token.role === "user"
                  ? <PersonalCabinet {...props} user={this.state.token}/>
                  : <Redirect to={{ pathname: '/404', state: { from: props.location } }} />}
              />
              <Route
                path="/panel"
                render={(props) => this.state.token.role === "admin"
                  ? <AdminPanel {...props} user={this.state.token}/>
                  : <Redirect to={{ pathname: '/404', state: { from: props.location } }} />}
              />
              <Route path="*" component={Error404} />
            </Switch>
            <Footer />
          </>
        </BrowserRouter>
      )
    } else {
      return (
        <div>preloader</div>
      )
    }
  }
}

const mapStateToProps = state => ({
  token: state.token
});

export default connect(
  mapStateToProps,
  state => ({ token: state.token }),
  dispatch => ({ toRedux: (data) => { dispatch(updateToken(data)) } }))(App)