<?php


class Auth
{
    public static function login($email, $password, $auth_key)
    {
        $connection = Connection::getConnectionFromFile($_SERVER['DOCUMENT_ROOT'] . '/../php/.ini');

        $user = UsersTable::getByEmail($email, $connection);

        if ($user) {
            $passwordHash = hash("sha256", md5($password) . md5($user['salt']));

            if ($passwordHash === $user['password']) {
                UsersTable::insertAuthKey($user['id'], $auth_key, $connection);

                echo json_encode([
                    'id' => $user['id'],
                    'email' => $user['email'],
                    'name' => $user['name'],
                    'surname' => $user['surname'],
                    'auth_key' => $user['auth_key']
                ]);
            }
            else {
                echo json_encode(['error' => 'Incorrect password']);
            }
        }
        else {
            echo json_encode(['error' => 'User not found']);
        }
    }

    public static function register($email, $password, $name, $surname)
    {
        $connection = Connection::getConnectionFromFile($_SERVER['DOCUMENT_ROOT'] . '/../php/.ini');
        $salt = Security::generateSalt();

        $passwordHash = hash('sha256', md5($password) . md5($salt));

        $status = UsersTable::insert($email, $passwordHash, $salt, $name, $surname, $connection);

        if ($status) {
            echo json_encode(['status' => 'User added']);
        }
        else {
            echo json_encode(['error' => 'Failed user added']);
        }
    }
}