<?php


class NewsTable
{
    public static function getAll($connection)
    {
        $sql = "SELECT * FROM `news`";

        return Database::getQueryMany($connection, $sql);
    }

    public static function getById($id, $connection)
    {
        $sql = "SELECT * FROM `news` WHERE `id` = '$id'";

        return Database::getQueryMany($connection, $sql);
    }

    /**
     * @param $post Post
     * @param $connection
     * @return bool
     */
    public static function insert($post, $connection)
    {
        $sql = "INSERT INTO `news` (`title`, `description`, `text`, `date`, `author`) VALUES (
        '" . $post->getTitle() . "',
        '" . $post->getDescription() . "',
        '" . $post->getText() . "',
        '" . $post->getDate() . "',
        '" . $post->getAuthor() . "'
        )";

        return mysqli_query($connection, $sql) or die(mysqli_error($connection));
    }

    /**
     * @param $post Post
     * @param $connection
     * @return bool
     */
    public static function update($post, $connection)
    {
        $id = $post->getId();
        $sql = "UPDATE `news` SET 
        `title` = '" . $post->getTitle() . "',
        `description` = '" . $post->getDescription() . "',
        `text` = '" . $post->getText() . "',
        `date` = '" . $post->getDate() . "',
        `author` = '" . $post->getAuthor() . "' WHERE `id` = '$id'";

        return mysqli_query($connection, $sql) or die(mysqli_error($connection));
    }

    public static function delete($id, $connection)
    {
        $sql = "DELETE FROM `news` WHERE `id` = '$id'";

        return mysqli_query($connection, $sql) or die(mysqli_error($connection));
    }
}