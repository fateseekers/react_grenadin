<?php


class UsersTable
{
    private static $sql;
    private static $tableName = 'users';

    public static function insert($email, $password, $salt, $name, $surname, $connection)
    {
        self::$sql = "INSERT INTO `" . self::$tableName . "` (`email`, `password`, `salt`, `name`, `surname`) VALUES ('$email', '$password', '$salt', '$name', '$surname')";

        return mysqli_query($connection, self::$sql) or die(mysqli_error($connection));
    }

    public static function insertAuthKey($id, $auth_key, $connection)
    {
        self::$sql = "UPDATE `" . self::$tableName . "` SET `auth_key` = $auth_key WHERE `id` = '$id'";

        return mysqli_query($connection, self::$sql);
    }

    public static function getByEmail($email, $connection)
    {
        self::$sql = "SELECT * FROM `users` WHERE `email` = '$email'";

        if ($query = mysqli_query($connection, self::$sql))
            return mysqli_fetch_assoc($query);
        else
            return null;
    }

    public static function setAccessToken($id, $token, $time, $connection)
    {
        return mysqli_query($connection, "UPDATE `users` SET `access_token` = '$token', `access_token_death` = '$time' WHERE `id` = '$id'");
    }

    public static function setRefreshToken($id, $token, $time, $connection)
    {
        return mysqli_query($connection, "UPDATE `users` SET `refresh_token` = '$token', `refresh_token_death` = '$time' WHERE `id` = '$id'");
    }
}