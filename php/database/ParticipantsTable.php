<?php


class ParticipantsTable
{
    public static function getAll($connection)
    {
        $sql = "SELECT * FROM `participants`";

        return Database::getQueryMany($connection, $sql);
    }

    public static function getByUserId($id, $connection)
    {
        $sql = "SELECT * FROM `participants` WHERE `user_id` = $id";

        return Database::getQueryMany($connection, $sql);
    }

    public static function getById($id, $connection)
    {
        $sql = "SELECT * FROM `participants` WHERE `id` = $id";

        return Database::getQueryMany($connection, $sql);
    }

    public static function getByStatus($status, $connection)
    {
        $sql = "SELECT * FROM `participants` WHERE `status` LIKE '$status'";

        return Database::getQueryMany($connection, $sql);
    }

    public static function getByType($type, $connection)
    {
        $sql = "SELECT * FROM `participants` WHERE `type` LIKE '$type'";

        return Database::getQueryMany($connection, $sql);
    }

    /**
     * @param $participant Participant
     * @param $connection
     * @return bool
     */
    public static function insert($participant, $connection)
    {
        $sql = "INSERT INTO `participants` (
        `full_company_name`,
        `web_site`,
        `inn`,
        `kpp`,
        `bik`,
        `pc`,
        `ks`,
        `bank`,
        `company_legal_address`,
        `company_mail_address`,
        `company_phone`,
        `company_fax`,
        `company_email`,
        `guide_fio`,
        `responsible_fio`,
        `responsible_email`,
        `responsible_position`,
        `responsible_phone`,
        `type_of_product`,
        `num_of_items`,
        `width`,
        `length`,
        `electricity_consumed`,
        `wishes`,
        `type`,
        `user_id`
        ) VALUES (
        '" . $participant->getFullCompanyName() . "',
        '" . $participant->getWebsite() . "',
        '" . $participant->getInn() . "',
        '" . $participant->getKpp() . "',
        '" . $participant->getBik() . "',
        '" . $participant->getPc() . "',
        '" . $participant->getKs() . "',
        '" . $participant->getBank() . "',
        '" . $participant->getCompanyLegalAddress() . "',
        '" . $participant->getCompanyMailAddress() . "',
        '" . $participant->getCompanyPhone() . "',
        '" . $participant->getCompanyFax() . "',
        '" . $participant->getCompanyEmail() . "',
        '" . $participant->getGuideFio() . "',
        '" . $participant->getResponsibleFio() . "',
        '" . $participant->getResponsibleEmail() . "',
        '" . $participant->getResponsiblePosition() . "',
        '" . $participant->getResponsiblePhone() . "',
        '" . $participant->getTypeOfProduct() . "',
        '" . $participant->getNumOfItems() . "',
        '" . $participant->getWidth() . "',
        '" . $participant->getLength() . "',
        '" . $participant->getElectricityConsumed() . "',
        '" . $participant->getWishes() . "',
        '" . $participant->getType() . "',
        '" . $participant->getUserId() . "'
        )";

        return mysqli_query($connection, $sql) or die(mysqli_error($connection));
    }

    public static function updateStatus($id, $status, $connection)
    {
        $sql = "UPDATE `participants` SET `status` = '$status' WHERE `id` = '$id'";

        return mysqli_query($connection, $sql) or die(mysqli_error($connection));
    }

    public static function delete($id, $connection)
    {
        return mysqli_query($connection, "DELETE FROM `participants` WHERE `id` = '$id'") or die(mysqli_error($connection));
    }
}