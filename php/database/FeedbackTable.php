<?php


class FeedbackTable
{
    public static function getAll($connection)
    {
        $sql = "SELECT * FROM `feedback`";

        return Database::getQueryMany($connection, $sql);
    }

    /**
     * @param $feedback Feedback
     * @param $connection
     * @return bool
     */
    public static function insert($feedback, $connection)
    {
        $sql = "INSERT INTO `feedback` (`name`, `email`, `message`, `date`) VALUES (
        '" . $feedback->getName() . "',
        '" . $feedback->getEmail() . "',
        '" . $feedback->getMessage() . "',
        '" . $feedback->getDate() . "'
        )";

        return mysqli_query($connection, $sql) or die(mysqli_error($connection));
    }
}