<?php


class Feedback
{
    private $id;
    private $name;
    private $email;
    private $message;
    private $date;

    /**
     * Feedback constructor.
     * @param $name
     * @param $email
     * @param $message
     */
    public function __construct($name, $email, $message, $date)
    {
        $this->name = $name;
        $this->email = $email;
        $this->message = $message;
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}