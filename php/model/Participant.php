<?php


class Participant
{
    private $id;
    private $fullCompanyName;
    private $website;
    private $inn;
    private $kpp;
    private $bik;
    private $pc;
    private $ks;
    private $bank;
    private $companyLegalAddress;
    private $companyMailAddress;
    private $companyPhone;
    private $companyFax;
    private $companyEmail;
    private $guideFio;
    private $responsibleFio;
    private $responsibleEmail;
    private $responsiblePosition;
    private $responsiblePhone;
    private $typeOfProduct;
    private $numOfItems;
    private $width;
    private $length;
    private $electricityConsumed;
    private $wishes;
    private $type;
    private $user_id;

    /**
     * Participant constructor.
     * @param $fullCompanyName
     * @param $website
     * @param $inn
     * @param $kpp
     * @param $bik
     * @param $pc
     * @param $ks
     * @param $bank
     * @param $companyLegalAddress
     * @param $companyMailAddress
     * @param $companyPhone
     * @param $companyFax
     * @param $companyEmail
     * @param $guideFio
     * @param $responsibleFio
     * @param $responsibleEmail
     * @param $responsiblePosition
     * @param $responsiblePhone
     * @param $typeOfProduct
     * @param $width
     * @param $length
     * @param $electricityConsumed
     * @param $wishes
     * @param $type
     * @param $user_id
     */
    public function __construct($fullCompanyName, $website, $inn, $kpp, $bik, $pc, $ks, $bank, $companyLegalAddress, $companyMailAddress, $companyPhone, $companyFax, $companyEmail, $guideFio, $responsibleFio, $responsibleEmail, $responsiblePosition, $responsiblePhone, $typeOfProduct, $numOfItems, $width, $length, $electricityConsumed, $wishes, $type, $user_id)
    {
        $this->fullCompanyName = $fullCompanyName;
        $this->website = $website;
        $this->inn = $inn;
        $this->kpp = $kpp;
        $this->bik = $bik;
        $this->pc = $pc;
        $this->ks = $ks;
        $this->bank = $bank;
        $this->companyLegalAddress = $companyLegalAddress;
        $this->companyMailAddress = $companyMailAddress;
        $this->companyPhone = $companyPhone;
        $this->companyFax = $companyFax;
        $this->companyEmail = $companyEmail;
        $this->guideFio = $guideFio;
        $this->responsibleFio = $responsibleFio;
        $this->responsibleEmail = $responsibleEmail;
        $this->responsiblePosition = $responsiblePosition;
        $this->responsiblePhone = $responsiblePhone;
        $this->typeOfProduct = $typeOfProduct;
        $this->numOfItems = $numOfItems;
        $this->width = $width;
        $this->length = $length;
        $this->electricityConsumed = $electricityConsumed;
        $this->wishes = $wishes;
        $this->type = $type;
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFullCompanyName()
    {
        return $this->fullCompanyName;
    }

    /**
     * @return mixed
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @return mixed
     */
    public function getInn()
    {
        return $this->inn;
    }

    /**
     * @return mixed
     */
    public function getKpp()
    {
        return $this->kpp;
    }

    /**
     * @return mixed
     */
    public function getBik()
    {
        return $this->bik;
    }

    /**
     * @return mixed
     */
    public function getPc()
    {
        return $this->pc;
    }

    /**
     * @return mixed
     */
    public function getKs()
    {
        return $this->ks;
    }

    /**
     * @return mixed
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @return mixed
     */
    public function getCompanyLegalAddress()
    {
        return $this->companyLegalAddress;
    }

    /**
     * @return mixed
     */
    public function getCompanyMailAddress()
    {
        return $this->companyMailAddress;
    }

    /**
     * @return mixed
     */
    public function getCompanyPhone()
    {
        return $this->companyPhone;
    }

    /**
     * @return mixed
     */
    public function getCompanyFax()
    {
        return $this->companyFax;
    }

    /**
     * @return mixed
     */
    public function getCompanyEmail()
    {
        return $this->companyEmail;
    }

    /**
     * @return mixed
     */
    public function getGuideFio()
    {
        return $this->guideFio;
    }

    /**
     * @return mixed
     */
    public function getResponsibleFio()
    {
        return $this->responsibleFio;
    }

    /**
     * @return mixed
     */
    public function getResponsibleEmail()
    {
        return $this->responsibleEmail;
    }

    /**
     * @return mixed
     */
    public function getResponsiblePosition()
    {
        return $this->responsiblePosition;
    }

    /**
     * @return mixed
     */
    public function getResponsiblePhone()
    {
        return $this->responsiblePhone;
    }

    /**
     * @return mixed
     */
    public function getTypeOfProduct()
    {
        return $this->typeOfProduct;
    }

    /**
     * @return mixed
     */
    public function getNumOfItems()
    {
        return $this->numOfItems;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @return mixed
     */
    public function getElectricityConsumed()
    {
        return $this->electricityConsumed;
    }

    /**
     * @return mixed
     */
    public function getWishes()
    {
        return $this->wishes;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }
}