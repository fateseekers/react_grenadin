<?php


class Post
{
    private $id;
    private $title;
    private $description;
    private $text;
    private $date;
    private $author;

    /**
     * Post constructor.
     * @param $title
     * @param $description
     * @param $text
     * @param $date
     * @param $author
     */
    public function __construct($title, $description, $text, $date, $author)
    {
        $this->title = $title;
        $this->description = $description;
        $this->text = $text;
        $this->date = $date;
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


}