<?php


class Security
{
    public static function protectPost($postItem)
    {
        return htmlspecialchars(trim($postItem));
    }

    public static function generateSalt()
    {
        $salt = '';
        $saltLength = 25;
        for($i = 0; $i < $saltLength; $i++) {
            $salt .= chr(mt_rand(33,126));
        }
        return $salt;
    }

    public static function validatePassword($password, $user_password, $user_salt)
    {
        return ($user_password === hash("sha256", md5($password) . md5($user_salt)));
    }

    public static function getSecretKey($path)
    {
        $data = parse_ini_file($path, true);

        return ($data) ? $data['token']['secret'] : new InvalidArgumentException();
    }
}