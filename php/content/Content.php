<?php


class Content
{
    /** Получение соединения с базой данных */
    private static function getConnection()
    {
        return Connection::getConnectionFromFile($_SERVER['DOCUMENT_ROOT'] . '/../php/.ini');
    }

    public static function getPagesList()
    {
        $connection = self::getConnection();

        if ($content = Database::getPages($connection))
            echo json_encode($content);
        else
            echo "{'error': 'Pages not found!'}";
    }
    
    public static function getContent($page)
    {
        $connection = self::getConnection();

        if ($content = Database::getContent($page, $connection))
            echo json_encode($content);
        else
            echo "{'error': 'Content not found!'}";
    }



    /**
     * Обработчики для заявок
     */

    public static function participantsGetAll()
    {
        $connection = self::getConnection();

        if ($content = ParticipantsTable::getAll($connection))
            echo json_encode($content);
        else
            echo "{'error': 'Participants not found!'}";
    }

    public static function participantsGetById($id)
    {
        $connection = self::getConnection();

        if ($content = ParticipantsTable::getById($id, $connection))
            echo json_encode($content);
        else
            echo "{'error': 'Participants not found!'}";
    }

    public static function participantsGetByUserId($id)
    {
        $connection = self::getConnection();

        if ($content = ParticipantsTable::getByUserId($id, $connection))
            echo json_encode($content);
        else
            echo "{'error': 'Participants not found!'}";
    }

    public static function participantsGetByStatus($status)
    {
        $connection = self::getConnection();

        if ($content = ParticipantsTable::getByStatus($status, $connection))
            echo json_encode($content);
        else
            echo "{'error': 'Participants not found!'}";
    }

    public static function participantsGetByType($type)
    {
        $connection = self::getConnection();

        if ($content = ParticipantsTable::getByType($type, $connection))
            echo json_encode($content);
        else
            echo "{'error': 'Participants not found!'}";
    }

    public static function participantsInsert($participant)
    {
        $connection = self::getConnection();

        if ($content = ParticipantsTable::insert($participant, $connection))
            echo "{'status': 'Participant added'}";
        else
            echo "{'error': 'Participant not added!'}";
    }

    public static function participantsUpdateStatus($id, $status)
    {
        $connection = self::getConnection();

        if ($content = ParticipantsTable::updateStatus($id, $status, $connection))
            echo "{'status': 'Participant updated'}";
        else
            echo "{'error': 'Participant not updated!'}";
    }

    public static function participantsDelete($id)
    {
        $connection = self::getConnection();

        if ($content = ParticipantsTable::delete($id, $connection))
            echo "{'status': 'Participant removed'}";
        else
            echo "{'error': 'Participant not removed!'}";
    }



    /**
     * Обработчики для обратной связи
     */

    public static function feedbackGetAll()
    {
        $connection = self::getConnection();

        if ($content = FeedbackTable::getAll($connection))
            echo json_encode($content);
        else
            echo "{'error': 'Feedback not found!'}";
    }

    public static function feedbackInsert($feedback)
    {
        $connection = self::getConnection();

        if ($content = FeedbackTable::insert($feedback, $connection))
            echo "{'status': 'Feedback added'}";
        else
            echo "{'error': 'Feedback not added!'}";
    }



    /**
     * Обработчики для новостей
     */

    public static function newsGetAll()
    {
        $connection = self::getConnection();

        if ($content = NewsTable::getAll($connection))
            echo json_encode($content);
        else
            echo "{'error': 'Participant not removed!'}";
    }

    public static function newsGetById($id)
    {
        $connection = self::getConnection();

        if ($content = NewsTable::getById($id, $connection))
            echo json_encode($content);
        else
            echo "{'error': 'Participant not removed!'}";
    }

    public static function newsInsert($post)
    {
        $connection = self::getConnection();

        if ($content = NewsTable::insert($post, $connection))
            echo "{'status': 'Post added'}";
        else
            echo "{'error': 'Post not added!'}";
    }

    public static function newsUpdate($post)
    {
        $connection = self::getConnection();

        if ($content = NewsTable::update($post, $connection))
            echo "{'status': 'Post updated'}";
        else
            echo "{'error': 'Post not updated!'}";
    }

    public static function newsDelete($id)
    {
        $connection = self::getConnection();

        if ($content = NewsTable::delete($id, $connection))
            echo "{'status': 'Post removed'}";
        else
            echo "{'error': 'Post not removed!'}";
    }
}