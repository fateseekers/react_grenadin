<?php


class Database
{
    private static $sql;

    public static function getQueryMany($connection, $sql)
    {
        $res = array();

        if ($query = mysqli_query($connection, $sql)) {
            while ($arr = mysqli_fetch_assoc($query)) {
                $res[] = $arr;
            }

            return $res;
        }
        else
            return null;
    }

    /**
     * @param $page
     * @param $block
     * @param $connection
     * @return array|null
     */
    public static function getContent($page, $connection)
    {
        self::$sql = "SELECT `block`,`content` FROM `content` WHERE `page` LIKE '$page'";

        $res = array();

        if ($query = mysqli_query($connection, self::$sql)) {
            while ($arr = mysqli_fetch_assoc($query)) {
                $res[] = $arr;
            }

            return $res;
        }
        else
            return null;
    }

    public static function getPages($connection)
    {
        self::$sql = "SELECT `page`,`block`,`content` FROM `content`";

        $res = array();

        if ($query = mysqli_query($connection, self::$sql)) {
            while ($arr = mysqli_fetch_assoc($query)) {
                $res[] = $arr;
            }

            return $res;
        }
        else
            return null;
    }


}